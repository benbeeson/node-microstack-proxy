const http = require("http")
const httpProxy = require("http-proxy");


function ProxyServer(config) {
    const proxy = httpProxy.createProxyServer({})
    const server = http.createServer((req, res) => {
        const serviceName = req.headers.host
        const redirect = config.services[serviceName]

        try
        {
            if(redirect)
            {
                const target = redirect.target
                console.log(`Redirecting ${req.url} to http://${target.host}:${target.port}`)

                proxy.web(req, res, {
                    target: redirect.target
                })
            }
            else
            {
                console.log(`Skipping request to ${req.url}`)

                proxy.web(req, res, {
                    target: 'http://${serviceName}'
                })
            }
        }
        catch(error) 
        {
            console.log(`Error redirecting request ${error.message}`)

            res.send({
                success: false,
                message: "Error proxying request"
            })
        }
    })

    this.start = () => {
        return new Promise((resolve) => {
            server.listen(config.server.port, () => {
                resolve()
            })
        })
    }
}


module.exports = ProxyServer
