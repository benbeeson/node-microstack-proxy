# MicroStack Proxy

Script for bringing up multiple NodeJS MicroStack enabled services setting up a local proxy for inter-service communication.

## Steps to Run

1. `cd` to the directory that contains all your microservices.
2. Checkout this repository into that directory.
3. Run `./node-microstack-proxy/run -s <root service> -d <service1>,<service2>` for example `./node-microstack-proxy/run -s api-service -d user-service,messages-service` 
4. -s is the name of the folder containing the main root service. This will be exposed under http://localhost:3000, use `-p <port_number>` to change this default.
5. -d is the list of service dependencies, these will be all brought up first.
6. A local proxy will be brought up and the all service will be setup to proxy omunication via this.
7. Each service will be setup to detect local file changes. After 2 seconds of no change after one or more changes, that service will be killed and brought up with the new code meaning you do not need to kill the script to restart.