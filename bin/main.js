const ProxyServer = require("../lib/proxyserver")
const config = require("../config")


function main() {
    const server = new ProxyServer(config)

    server.start()
        .then(() => {
            console.log(`microstack-proxy started on ${config.server.port}`)
        })
}

main()
