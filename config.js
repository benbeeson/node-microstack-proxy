require("dotenv").config()

module.exports = {
    server: {
        port: process.env.PORT || 80
    },
    services: {}
}
